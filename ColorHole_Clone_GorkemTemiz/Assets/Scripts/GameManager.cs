﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject mainCamera;
    
    [Header("Variables for The First Part of The Level")]
    public GameObject gameField1;
    public GameObject holeParent1;
    public int obstaclesCountFirstPart;
    
    [Header("Variables for The Connector Part of The Level")]
    public GameObject fieldConnector;
    public GameObject holeParentConn;
    
    [Header("Variables for The Second Part of The Level")]
    public GameObject gameField2;
    public GameObject holeParent2;
    public int obstaclesCountSecondPart;

    [Header("Variables for managing UI")]
    public GameObject inGamePanel;
    public GameObject scoreText;
    public GameObject failPanel;
    public GameObject nextLevelPanel;


    [HideInInspector] public int score;
    [HideInInspector] public int obstacleCounter;
    private bool coroutineTriggered;

    void Start()
    {
        coroutineTriggered = false;
    }

     
    void Update()
    {
        scoreText.GetComponent<TextMeshProUGUI>().text = score.ToString(); // sets scoreText on the inGamePanel.

        if (obstacleCounter == obstaclesCountFirstPart) // when all obstacles in the first part are collected, starts the transition routine.
        {
            StartCoroutine(MakeTransition_Co());
        }
        else if (obstacleCounter == obstaclesCountSecondPart) // when all collectables fall from hole, "next level panel" is shown.
        {
            nextLevelPanel.SetActive(true);
        }
    }

    IEnumerator MakeTransition_Co() // the function which manages the transition sequence
    {
        if(!coroutineTriggered)
        {
            holeParent1.transform.DOMoveX(0, 0.5f);
            yield return new WaitForSeconds(0.5f);
            holeParent1.transform.DOMoveZ(5, 0.5f).SetEase(Ease.OutSine);
            mainCamera.transform.DOMoveZ(3, 0.5f).SetEase(Ease.OutSine);
            yield return new WaitForSeconds(0.6f);
            holeParentConn.SetActive(true);
            holeParent1.SetActive(false);
            holeParentConn.transform.DOMoveZ(25, 7).SetEase(Ease.OutSine);
            mainCamera.transform.DOMoveZ(20, 7).SetEase(Ease.OutSine);
            yield return new WaitForSeconds(7.2f);
            holeParent2.SetActive(true);
            holeParentConn.SetActive(false);
            coroutineTriggered = true;
        }
    }

    public void GoNextLevel() // the function of the button which directs to next level at the end of the level.
    {
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % 3);
    }

    public void RetryCurrentLevel() // the function of the button which restart the current level(the button is a child of "failPanel").
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
