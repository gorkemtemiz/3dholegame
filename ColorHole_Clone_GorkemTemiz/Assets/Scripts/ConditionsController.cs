﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using TMPro;
using UnityEngine;

public class ConditionsController : MonoBehaviour
{
    public GameManager gameManager;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag== "CoolObject") //"CoolObject" tag is used for the collectable objects. All this kind of inGame-Objects are also called "Obstacles" in this project.
        {
            gameManager.obstacleCounter++;
            gameManager.score += 10;
            Debug.Log(gameManager.obstacleCounter);
        }
        else if(other.tag == "LameObject") //"LameObject" tag is used for the not-collectable objects. These are also in the group of "Obstacles".
        {
            gameManager.inGamePanel.SetActive(false);
            gameManager.failPanel.SetActive(true);
        }
        
        Destroy(other.gameObject);
    }
}
