﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnChangePosition : MonoBehaviour
{
    public PolygonCollider2D hole2Dcollider;
    public PolygonCollider2D ground2Dcollider;
    public MeshCollider GeneratedMeshCollider;
    public Collider GroundCollider;
    public float initialScale = 0.5f;
    Mesh GeneratedMesh;
    Vector3 hole2DPos;


    private void Start()
    {
        GameObject[] AllGOs = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach (var go in AllGOs)
        {
            if (go.layer == LayerMask.NameToLayer("Obstacles"))
            {
                Physics.IgnoreCollision(go.GetComponent<Collider>(), GeneratedMeshCollider, true);
            }
        }
    }

    private void Update()
    {
        MoveWithTouch();
    }

    private void FixedUpdate()
    {
        if (transform.hasChanged == true)    //hole parentin pozisyon degisimine gore, hole 2d colliderin pozisyonunu yeni pozisyona tasiyan kod parcasi 
        {
            transform.hasChanged = false;
            hole2Dcollider.transform.position = new Vector2(transform.position.x, transform.position.z);
            hole2Dcollider.transform.localScale = transform.localScale * initialScale;
            MakeHole2D();
            Make3DMeshCollider();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        Physics.IgnoreCollision(other, GroundCollider, true);
        Physics.IgnoreCollision(other, GeneratedMeshCollider, false);
    }

    private void OnTriggerExit(Collider other)
    {
        Physics.IgnoreCollision(other, GroundCollider, false);
        Physics.IgnoreCollision(other, GeneratedMeshCollider, true);
    }

    private void MakeHole2D() //the function which gets world positions of the Hole2dCollider's nodes, and creates a new path with these nodes on the ground2dCollider 
    {
        Vector2[] PointPositions = hole2Dcollider.GetPath(0);

        for(int i = 0; i < PointPositions.Length; i++)
        {
            hole2DPos = hole2Dcollider.transform.TransformPoint(PointPositions[i]);
            if(transform.position.z >= 20f)
            {
                hole2DPos = new Vector3(hole2DPos.x, hole2DPos.y - transform.parent.position.z, hole2DPos.z + transform.parent.position.z);
                PointPositions[i] = hole2DPos;
            }
            else
            {
                PointPositions[i] = hole2DPos;
            }
        }

        ground2Dcollider.pathCount = 2;
        ground2Dcollider.SetPath(1, PointPositions);
    }

    private void Make3DMeshCollider() 
    {
        if(GeneratedMesh != null)
        {
            Destroy(GeneratedMesh);
        }
        GeneratedMesh = ground2Dcollider.CreateMesh(true, true);
        GeneratedMeshCollider.sharedMesh = GeneratedMesh;
    }

    public void MoveWithTouch() // the controls function for the mobile platforms
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                Vector2 deltaPos = touch.deltaPosition;
                transform.position = new Vector3(transform.position.x + (deltaPos.x / 100), transform.position.y, transform.position.z + (deltaPos.y / 100));
            }
        }
    }

    public void Move(BaseEventData myEvent) // the controls function for the testing on the editor
    {
        if (((PointerEventData)myEvent).pointerCurrentRaycast.isValid)
        {
            transform.position = ((PointerEventData)myEvent).pointerCurrentRaycast.worldPosition;
        }
    }
}
