﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleCollisionController : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if(other.tag != "HolePart")
        {
            other.GetComponent<Rigidbody>().AddForce(-transform.up, ForceMode.Force);
        }
    }
}
